# NixOS Pre-Install Script

## Overview
This script automates the partitioning process for NixOS installations. It is designed to be executed from a Live USB environment.

## Features
- Sets up correct partitions for NixOS
- Utilizes experimental Nix features

## Prerequisites
- Live USB with NixOS
- `nix-command` and `flakes` experimental features enabled

## Usage
Run the following command to execute the script:

```bash
nix --extra-experimental-features "nix-command flakes" run gitlab:usmcamp0811/pre-install-script#pre-install-script -- --hdd /dev/nvme0n1 --vault-addr https://vault.lan.aicampground.com
```

