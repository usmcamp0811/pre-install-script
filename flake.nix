{
  description = "An over-engineered Hello World in bash";

  # Nixpkgs / NixOS version to use.
  inputs.nixpkgs.url = "nixpkgs/nixos-21.05";

  outputs = { self, nixpkgs }:
    let

      # to work with older version of flakes
      lastModifiedDate = self.lastModifiedDate or self.lastModified or "19700101";

      # Generate a user-friendly version number.
      version = builtins.substring 0 8 lastModifiedDate;

      # System types to support.
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];

      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; overlays = [ self.overlay ]; });

    in

    {

      # A Nixpkgs overlay.
      overlay = final: prev: {

        pre-install-script = with final; stdenv.mkDerivation rec {
          name = "pre-install-script-${version}";

          unpackPhase = ":";

          installPhase =
          let
            zfs_script = writeShellScript "zfs-pre-install-script.sh" ''
              # This script automates the setup of disk partitioning, encryption, and mounting
              # for a BTRFS-based Linux installation.
              set -e
              set -x
              # Initialize variables
              root_hdd_name=""
              vault_path="secret/campground/luks"  # Default Vault path
              export VAULT_ADDR='https://vault.lan.aicampground.com'

              # Parse short and long options
              while :; do
                  case $1 in
                      -h|--hdd)
                          if [ "$2" ]; then
                              root_hdd_name=$2
                              shift
                          else
                              echo 'ERROR: "--root-hdd-name" requires a non-empty option argument.'
                              exit 1
                          fi
                          ;;
                      -v|--vault-path)
                          if [ "$2" ]; then
                              vault_path=$2
                              shift
                          else
                              echo 'ERROR: "--vault-path" requires a non-empty option argument.'
                              exit 1
                          fi
                          ;;
                      -a|--vault-addr)
                          if [ "$2" ]; then
                              export VAULT_ADDR="$2"
                              shift
                          else
                              echo 'ERROR: "--vault-addr" requires a non-empty option argument.'
                              exit 1
                          fi
                          ;;
                      --)
                          shift
                          break
                          ;;
                      -*)
                          echo "Unknown option: $1"
                          exit 1
                          ;;
                      *)
                          break
                  esac
                  shift
              done

              # Determine if the HDD uses 'p' for partition naming based on NVMe or not
              if [[ $root_hdd_name != "/dev/nvme0n1" ]]; then
                  part_name=""
              else
                  part_name="p"
              fi

              hdd_name=$root_hdd_name$part_name

              # Function to retrieve a secret from HashiCorp Vault
              get_vault_secret() {
                  local secret_path="$1"
                  local key="$2"
                  
                  # Retrieve secret using vault read command
                  local secret_value=''$(${pkgs.vault}/bin/vault read -field=$key $secret_path)
                  echo "$secret_value"
              }

              echo "This is your Vault:"
              echo $VAULT_ADDR
              # Prompt for Vault login
              echo "Please login to Vault..."
              ${pkgs.vault}/bin/vault login || { echo "Vault login failed."; exit 1; }

              # Read the LUKS passphrase from HashiCorp Vault
              luks_passphrase=''$(get_vault_secret "$vault_path" "passphrase")

              # Retrieve the LUKS keyfile from Vault and save it to a file
              luks_keyfile_value=''$(get_vault_secret "$vault_path" "keyfile")
              luks_keyfile="luks.key"
              echo "$luks_keyfile_value" > "$luks_keyfile"



              # Function to encrypt the root partition using LUKS
              encrypt_root_partition() {
                  printf '%s' "$luks_passphrase" | sudo -S cryptsetup --batch-mode -c aes-xts-plain64 --use-random luksFormat "''${hdd_name}2"
                  printf '%s' "$luks_passphrase" | sudo -S cryptsetup luksAddKey "''${hdd_name}2" "$luks_keyfile"
                  printf '%s' "$luks_passphrase" | sudo -S cryptsetup luksOpen "''${hdd_name}2" luks
              }

              # Function to create BTRFS subvolumes and mount them
              mount_and_create_subvolumes() {
                  local luks_device="$1"
                  
                  # Mount the LUKS device
                  sudo mount "/dev/mapper/luks" /mnt

                  # Create BTRFS subvolumes
                  for subvol in "@" "@home" "@nix" "@persist" "@.snapshots"
                  do
                      sudo btrfs su cr "/mnt/$subvol"
                  done

                  # Unmount and then remount the LUKS device with specific options
                  sudo umount /mnt
                  sudo mount -o noatime,compress=lzo,subvol=@ "/dev/mapper/luks" /mnt
                  sudo mkdir -p /mnt/boot /mnt/home /mnt/nix /mnt/.snapshots /mnt/persist
                  sudo mount -o noatime,compress=lzo,space_cache,subvol=@home "/dev/mapper/luks" /mnt/home
                  sudo mount -o noatime,compress=lzo,space_cache,subvol=@persist "/dev/mapper/luks" /mnt/persist
                  sudo mount -o noatime,compress=lzo,space_cache,subvol=@.snapshots "/dev/mapper/luks" /mnt/.snapshots
                  sudo mount -o nodatacow,subvol=@nix "/dev/mapper/luks" /mnt/nix
              }

              # Function to wipe and partition the root HDD
              zap_and_partition_hdd() {
                  echo "Inputs: 1 => $1 2 => $2"
                  cmds=("sgdisk --clear $1 -Z" "sgdisk --clear $1 -o" "sgdisk -n=1:0:+1G $1" "sgdisk -N=2 $1" "wipefs -a ''${2}1" "wipefs -a ''${2}2")

                  echo "Commands to execute: ''${cmds[@]}"
                  for cmd in "''${cmds[@]}"; do
                      sudo -S $(echo $cmd)
                  done
              }

              # Perform HDD partitioning and setup
              zap_and_partition_hdd $root_hdd_name $hdd_name
              encrypt_root_partition
              sudo -S mkfs.vfat -F32 "''${hdd_name}1"
              sudo -S mkfs.btrfs /dev/mapper/luks
              mount_and_create_subvolumes "/dev/mapper/luks"
              sudo -S mount "''${hdd_name}1" /mnt/boot
            '';
          in
            ''
              mkdir -p $out/bin
              cp ${script} $out/bin/pre-install-script
            '';
            btrfs_script = writeShellScript "btrfs-pre-install-script.sh" ''
              # This script automates the setup of disk partitioning, encryption, and mounting
              # for a BTRFS-based Linux installation.
              set -e
              set -x
              # Initialize variables
              root_hdd_name=""
              vault_path="secret/campground/luks"  # Default Vault path
              export VAULT_ADDR='https://vault.lan.aicampground.com'

              # Parse short and long options
              while :; do
                  case $1 in
                      -h|--hdd)
                          if [ "$2" ]; then
                              root_hdd_name=$2
                              shift
                          else
                              echo 'ERROR: "--root-hdd-name" requires a non-empty option argument.'
                              exit 1
                          fi
                          ;;
                      -v|--vault-path)
                          if [ "$2" ]; then
                              vault_path=$2
                              shift
                          else
                              echo 'ERROR: "--vault-path" requires a non-empty option argument.'
                              exit 1
                          fi
                          ;;
                      -a|--vault-addr)
                          if [ "$2" ]; then
                              export VAULT_ADDR="$2"
                              shift
                          else
                              echo 'ERROR: "--vault-addr" requires a non-empty option argument.'
                              exit 1
                          fi
                          ;;
                      --)
                          shift
                          break
                          ;;
                      -*)
                          echo "Unknown option: $1"
                          exit 1
                          ;;
                      *)
                          break
                  esac
                  shift
              done

              # Determine if the HDD uses 'p' for partition naming based on NVMe or not
              if [[ $root_hdd_name != "/dev/nvme0n1" ]]; then
                  part_name=""
              else
                  part_name="p"
              fi

              hdd_name=$root_hdd_name$part_name

              # Function to retrieve a secret from HashiCorp Vault
              get_vault_secret() {
                  local secret_path="$1"
                  local key="$2"
                  
                  # Retrieve secret using vault read command
                  local secret_value=''$(${pkgs.vault}/bin/vault read -field=$key $secret_path)
                  echo "$secret_value"
              }

              echo "This is your Vault:"
              echo $VAULT_ADDR
              # Prompt for Vault login
              echo "Please login to Vault..."
              ${pkgs.vault}/bin/vault login || { echo "Vault login failed."; exit 1; }

              # Read the LUKS passphrase from HashiCorp Vault
              luks_passphrase=''$(get_vault_secret "$vault_path" "passphrase")

              # Retrieve the LUKS keyfile from Vault and save it to a file
              luks_keyfile_value=''$(get_vault_secret "$vault_path" "keyfile")
              luks_keyfile="luks.key"
              echo "$luks_keyfile_value" > "$luks_keyfile"



              # Function to encrypt the root partition using LUKS
              encrypt_root_partition() {
                  printf '%s' "$luks_passphrase" | sudo -S cryptsetup --batch-mode -c aes-xts-plain64 --use-random luksFormat "''${hdd_name}2"
                  printf '%s' "$luks_passphrase" | sudo -S cryptsetup luksAddKey "''${hdd_name}2" "$luks_keyfile"
                  printf '%s' "$luks_passphrase" | sudo -S cryptsetup luksOpen "''${hdd_name}2" luks
              }

              # Function to create BTRFS subvolumes and mount them
              mount_and_create_subvolumes() {
                  local luks_device="$1"
                  
                  # Mount the LUKS device
                  sudo mount "/dev/mapper/luks" /mnt

                  # Create BTRFS subvolumes
                  for subvol in "@" "@home" "@nix" "@persist" "@.snapshots"
                  do
                      sudo btrfs su cr "/mnt/$subvol"
                  done

                  # Unmount and then remount the LUKS device with specific options
                  sudo umount /mnt
                  sudo mount -o noatime,compress=lzo,subvol=@ "/dev/mapper/luks" /mnt
                  sudo mkdir -p /mnt/boot /mnt/home /mnt/nix /mnt/.snapshots /mnt/persist
                  sudo mount -o noatime,compress=lzo,space_cache,subvol=@home "/dev/mapper/luks" /mnt/home
                  sudo mount -o noatime,compress=lzo,space_cache,subvol=@persist "/dev/mapper/luks" /mnt/persist
                  sudo mount -o noatime,compress=lzo,space_cache,subvol=@.snapshots "/dev/mapper/luks" /mnt/.snapshots
                  sudo mount -o nodatacow,subvol=@nix "/dev/mapper/luks" /mnt/nix
              }

              # Function to wipe and partition the root HDD
              zap_and_partition_hdd() {
                  echo "Inputs: 1 => $1 2 => $2"
                  cmds=("sgdisk --clear $1 -Z" "sgdisk --clear $1 -o" "sgdisk -n=1:0:+1G $1" "sgdisk -N=2 $1" "wipefs -a ''${2}1" "wipefs -a ''${2}2")

                  echo "Commands to execute: ''${cmds[@]}"
                  for cmd in "''${cmds[@]}"; do
                      sudo -S $(echo $cmd)
                  done
              }

              # Perform HDD partitioning and setup
              zap_and_partition_hdd $root_hdd_name $hdd_name
              encrypt_root_partition
              sudo -S mkfs.vfat -F32 "''${hdd_name}1"
              sudo -S mkfs.btrfs /dev/mapper/luks
              sudo parted $root_hdd_name -- set 1 esp on
              mount_and_create_subvolumes "/dev/mapper/luks"
              sudo -S mount "''${hdd_name}1" /mnt/boot
            '';
          in
            ''
              mkdir -p $out/bin
              cp ${script} $out/bin/pre-install-script
            '';
        };

      };

      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        {
          inherit (nixpkgsFor.${system}) pre-install-script;
        });

      # The default package for 'nix build'. This makes sense if the
      # flake provides only one package or there is a clear "main"
      # package.
      defaultPackage = forAllSystems (system: self.packages.${system}.pre-install-script);

      # A NixOS module, if applicable (e.g. if the package provides a system service).
      nixosModules.btrfs-pre-install-script =
        { pkgs, ... }:
        {
          nixpkgs.overlays = [ self.overlay ];

          environment.systemPackages = [ pkgs.btrfs-pre-install-script ];

          #systemd.services = { ... };
        };

      nixosModules.zfs-pre-install-script =
        { pkgs, ... }:
        {
          nixpkgs.overlays = [ self.overlay ];

          environment.systemPackages = [  pkgs.zfs-pre-install-script ];

          #systemd.services = { ... };
        };

      # Tests run by 'nix flake check' and by Hydra.
      checks = forAllSystems
        (system:
          with nixpkgsFor.${system};

          {
            inherit (self.packages.${system}) pre-install-script;

            # Additional tests, if applicable.
            test = stdenv.mkDerivation {
              name = "pre-install-script-test-${version}";

              buildInputs = [ pre-install-script ];

              unpackPhase = "true";

              buildPhase = ''
                echo 'running some integration tests'
                [[ $(pre-install-script) = 'Hello Nixers!' ]]
              '';

              installPhase = "mkdir -p $out";
            };
          }

          // lib.optionalAttrs stdenv.isLinux {
            # A VM test of the NixOS module.
            vmTest =
              with import (nixpkgs + "/nixos/lib/testing-python.nix") {
                inherit system;
              };

              makeTest {
                nodes = {
                  client = { ... }: {
                    imports = [ self.nixosModules.pre-install-script ];
                  };
                };

                testScript =
                  ''
                    start_all()
                    client.wait_for_unit("multi-user.target")
                    client.succeed("pre-install-script")
                  '';
              };
          }
        );

    };
}
